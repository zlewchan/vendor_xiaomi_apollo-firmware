PRODUCT_COPY_FILES += \
        vendor/xiaomi/apollo-firmware/radio/abl.elf:install/firmware-update/abl.elf \
        vendor/xiaomi/apollo-firmware/radio/aop.mbn:install/firmware-update/aop.mbn \
        vendor/xiaomi/apollo-firmware/radio/BTFM.bin:install/firmware-update/BTFM.bin \
        vendor/xiaomi/apollo-firmware/radio/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
        vendor/xiaomi/apollo-firmware/radio/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
        vendor/xiaomi/apollo-firmware/radio/devcfg.mbn:install/firmware-update/devcfg.mbn \
        vendor/xiaomi/apollo-firmware/radio/dspso.bin:install/firmware-update/dspso.bin \
        vendor/xiaomi/apollo-firmware/radio/featenabler.mbn:install/firmware-update/featenabler.mbn \
        vendor/xiaomi/apollo-firmware/radio/hyp.mbn:install/firmware-update/hyp.mbn \
        vendor/xiaomi/apollo-firmware/radio/km4.mbn:install/firmware-update/km4.mbn \
        vendor/xiaomi/apollo-firmware/radio/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
        vendor/xiaomi/apollo-firmware/radio/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
        vendor/xiaomi/apollo-firmware/radio/storsec.mbn:install/firmware-update/storsec.mbn \
        vendor/xiaomi/apollo-firmware/radio/tz.mbn:install/firmware-update/tz.mbn \
        vendor/xiaomi/apollo-firmware/radio/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
        vendor/xiaomi/apollo-firmware/radio/xbl_4.elf:install/firmware-update/xbl_4.elf \
        vendor/xiaomi/apollo-firmware/radio/xbl_5.elf:install/firmware-update/xbl_5.elf \
        vendor/xiaomi/apollo-firmware/radio/xbl_config_4.elf:install/firmware-update/xbl_config_4.elf \
        vendor/xiaomi/apollo-firmware/radio/xbl_config_5.elf:install/firmware-update/xbl_config_5.elf
